package es.acm.generando.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import es.acm.generando.security.utils.Constants;
import es.acm.generando.security.utils.TokenProvider;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	public JWTAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		
		String headerAuthorization = req.getHeader(Constants.HEADER_AUTHORIZACION_KEY);
		
		if (StringUtils.isEmpty(headerAuthorization) || !headerAuthorization.startsWith(Constants.TOKEN_BEARER_PREFIX)) {
			chain.doFilter(req, res);
			return;
		}
		
		final String token = headerAuthorization.replace(Constants.TOKEN_BEARER_PREFIX + " ", "");
		
		UsernamePasswordAuthenticationToken authentication = TokenProvider.generateAuthentication(token);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}

	
	

	
}
