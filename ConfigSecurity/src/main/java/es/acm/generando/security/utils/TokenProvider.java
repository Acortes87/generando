package es.acm.generando.security.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.StringUtils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenProvider {

	private TokenProvider() {

	}

	/**
	 * Método que genera el token
	 * @param auth
	 * @return
	 */
	public static String createToken(Authentication auth) {
		
		final String authorities = auth.getAuthorities().stream().map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(","));

		String token = Jwts.builder().setIssuedAt(new Date()).setIssuer(Constants.ISSUER_INFO)
				.setSubject(((User) auth.getPrincipal()).getUsername()).claim(Constants.AUTHORITIES_KEY, authorities)
				.setExpiration(new Date(System.currentTimeMillis() + Constants.TOKEN_EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, Constants.SUPER_SECRET_KEY).compact();
		return token;
	}
	
	public static UsernamePasswordAuthenticationToken generateAuthentication(String token) {
		// Se procesa el token y se recupera el usuario.

		if (!StringUtils.isEmpty(token)) {
			
			return new UsernamePasswordAuthenticationToken(getUserName(token), null, getAuthorities(token));
		}
		return null;
	}
	
	/**
	 * Método que procesa el token y recupera el usuario
	 * @param token
	 * @return
	 */
	public static String getUserName(String token) {
		String user = Jwts.parser()
					.setSigningKey(Constants.SUPER_SECRET_KEY)
					.parseClaimsJws(token)
					.getBody()
					.getSubject();
		return user;
	}
	
	/**
	 * Obtiene los roles que existen en el token
	 * @param token
	 * @return
	 */
	private static Collection<SimpleGrantedAuthority> getAuthorities(String token){
		
		String roles = Jwts.parser()
						.setSigningKey(Constants.SUPER_SECRET_KEY)
						.parseClaimsJws(token)
						.getBody()
						.get(Constants.AUTHORITIES_KEY).toString();
		
		final Collection<SimpleGrantedAuthority> authorities =
				Arrays.stream(roles.split(","))
						.map(SimpleGrantedAuthority::new)
						.collect(Collectors.toList());
		
		return authorities;
		
	}
	
	
	

}
