package es.acm.generando.security.utils;

public class Constants {

	// Spring Security

	public static final String LOGIN_URL = "/login";
	public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
	public static final String TOKEN_BEARER_PREFIX = "Bearer ";

	// JWT
	public static final String AUTHORITIES_KEY = "CLAIM_TOKEN";
	public static final String ISSUER_INFO = "http://generando.es/";
	public static final String SUPER_SECRET_KEY = "6789";
	public static final long TOKEN_EXPIRATION_TIME = 86_400_000; // 1 day

}
