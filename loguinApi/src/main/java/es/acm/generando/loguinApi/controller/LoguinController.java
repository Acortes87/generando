package es.acm.generando.loguinApi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.acm.generando.loguinApi.dto.UsuarioDto;
import es.acm.generando.loguinApi.mappers.UsuarioMapper;
import es.acm.generando.loguinApi.model.Usuario;
import es.acm.generando.loguinApi.service.impl.LoguinService;

@RestController
@RequestMapping("/users")
public class LoguinController {

	@Autowired
	LoguinService loguinService;

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping()
	public ResponseEntity<List<UsuarioDto>> findAll(@RequestHeader(value = "Authorization") String authorization) {

		// String username
		// =TokenProvider.getUserName(authorization.replace(Constants.TOKEN_BEARER_PREFIX
		// + " ", ""));

		List<Usuario> as = loguinService.getAllUsuario();

//		List <UsuarioDto> response= as.stream().map(UsuarioDtoMapper::build).collect(Collectors.toList());

		List<UsuarioDto> responss = UsuarioMapper.convertList(as, UsuarioMapper::usuarioEntyToDto);

		// Usuario asd = loguinService.getUsuarioByEmail(username);

		return ResponseEntity.ok().body(responss);
	}

	@GetMapping("/{id}")
	public ResponseEntity<UsuarioDto> getUsuario(@PathVariable long id) {

		return null;
	}

}
