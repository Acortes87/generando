package es.acm.generando.loguinApi.mappers;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import es.acm.generando.loguinApi.dto.UsuarioDto;
import es.acm.generando.loguinApi.model.Usuario;


public class UsuarioMapper {

	public  static UsuarioDto usuarioEntyToDto (Usuario usuario){
		
		UsuarioDto usuarioDto = new UsuarioDto();
		
		usuarioDto.setApell1(usuario.getApell1());
		usuarioDto.setApell2(usuario.getApell2());
		usuarioDto.setCodIdenti(usuario.getCodIdenti());
		usuarioDto.setCodUsu(usuario.getCodUsu());
		usuarioDto.setDireccion(usuario.getDireccion());
		usuarioDto.setIdUsu(usuario.getIdUsu());
		usuarioDto.setNombre(usuario.getNombre());
//		usuarioDto.setPass(usuario.getPass());
				
		return usuarioDto;	
		
	}
	
	@SuppressWarnings("unchecked")
	public static <T, R> List<R> convertList (List<T> lstUsuIn, Function<? super T, ? extends R> mapper){
		
		return (List<R>) lstUsuIn.stream().map(mapper).collect(Collectors.toList());
		
	}
	
	public  static Usuario usuarioDtoToEnty(UsuarioDto usuario){
		
		Usuario usuarioDto = new Usuario();
			
			usuarioDto.setApell1(usuario.getApell1());
			usuarioDto.setApell2(usuario.getApell2());
			usuarioDto.setCodIdenti(usuario.getCodIdenti());
			usuarioDto.setCodUsu(usuario.getCodUsu());
			usuarioDto.setDireccion(usuario.getDireccion());
			usuarioDto.setIdUsu(usuario.getIdUsu());
			usuarioDto.setNombre(usuario.getNombre());
			usuarioDto.setPassword(usuario.getPassword());
					
			return usuarioDto;	
		
	}
	
	public  static UsuarioDto usuarioEntyToDtoR (Usuario usuario){
		
		UsuarioDto usuarioDto = new UsuarioDto();
		
		usuarioDto.setApell1(usuario.getApell1());
		usuarioDto.setApell2(usuario.getApell2());
		usuarioDto.setCodIdenti(usuario.getCodIdenti());
//		usuarioDto.setCodUsu(usuario.getCodUsu());
//		usuarioDto.setDireccion(usuario.getDireccion());
		usuarioDto.setIdUsu(usuario.getIdUsu());
		usuarioDto.setNombre(usuario.getNombre());
//		usuarioDto.setPass(usuario.getPass());
				
		return usuarioDto;	
		
	}
	
	
}
