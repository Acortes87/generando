package es.acm.generando.loguinApi.mappers;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import es.acm.generando.loguinApi.model.Role;
import es.acm.generando.loguinApi.model.Usuario;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserDetailsMapper {

	public static UserDetails build(Usuario user) {
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), getAuthorities(user));
	}

	private static Set<? extends GrantedAuthority> getAuthorities(Usuario retrievedUser) {
		List<Role> roles = retrievedUser.getRoles();

		Set<SimpleGrantedAuthority> authorities = new HashSet<>();

		roles.forEach(role -> authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getCodRole())));

		return authorities;
	}
}
