package es.acm.generando.loguinApi.service.impl;

import java.util.List;

import es.acm.generando.loguinApi.model.Usuario;

public interface LoguinService {
	
	public Usuario getUsuarioByEmail(String email);
	public List<Usuario> getAllUsuario();

}
