package es.acm.generando.loguinApi.service.impl;

import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.JoinType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.acm.generando.loguinApi.dao.UsuarioRepository;
import es.acm.generando.loguinApi.mappers.UserDetailsMapper;
import es.acm.generando.loguinApi.model.Usuario;

@Service
@Transactional
public class LoguinServiceImpl implements LoguinService, UserDetailsService {

	@Autowired
	UsuarioRepository usuario;

	public Usuario getUsuarioByEmail(String userName) {

		Usuario usu = usuario.findOne(findUserBy("userName", userName));

		return usu;
		
	}
	
	@Override
	public List<Usuario> getAllUsuario() {

		return usuario.findAll();
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		//UserDetails userDetail = new ;
		
		Usuario usu = usuario.findOne(findUserBy("userName",username, "roles")); //, "tipoEntidadBean"
		
		return UserDetailsMapper.build(usu);
	}
	
	private <T> Specification<T> findUserBy(final String key, final String value, final String... fetchs) {
		return (rt, qry, cb) -> {
			if(fetchs!=null && fetchs.length>0) 
				Arrays.stream(fetchs).forEach(p-> rt.fetch(p, JoinType.INNER));
			return cb.and(cb.like(cb.upper(rt.get(key)), value.toUpperCase()));
		};
	}
	
	@SuppressWarnings("unused")
	private <T> Specification<T> findUserBy(final String key, final Double value, final String... fetchs) {
		return (rt, qry, cb) -> {
			Arrays.stream(fetchs).forEach(p-> rt.fetch(p, JoinType.INNER));
			return cb.and(cb.equal(cb.upper(rt.get(key)), value));
		};
	}

	
//	private <T> Specification<T> addWhereAnd(final String... email) {
//
//		Specifications<T> predicados = Specifications.where(null);
//
//		// email
//		Arrays.stream(email).forEach(param -> {
//			predicados.and((rt, qry, cb) -> {
//				rt.fetch("roles", JoinType.INNER);
//				return cb.and(cb.like(cb.upper(rt.get("email")), param.toUpperCase()));
//			});
//		});
//
//		return predicados;
//	}
	
	/**
	 * Metodo que recibe un map con campo/valor para unirlos en un where
	 * @param <T>
	 * @param params
	 * @return
	 */
//	private <T> Specification<T> addLikeOrEqualsWhereAndMap(final Map<String, Object> params) {
//
//		Specifications<T> predicados = Specifications.where(null);
//		//predicados.get
//
//		params.entrySet().parallelStream().forEach(a -> {
//			
//			if(a.getValue() instanceof String) {
//				predicados.and((rt, qry, cb) -> {
//					rt.fetch("roles", JoinType.INNER);
//					return cb.and(cb.like(cb.upper(rt.get(a.getKey())), a.getValue().toString().toUpperCase()));
//				});
//			}else {
//				predicados.and((rt, qry, cb) -> {
//					rt.fetch("roles", JoinType.INNER);
//					return cb.and(cb.equal(cb.upper(rt.get(a.getKey())), a.getValue()));
//				});
//			}
//		});
//
//		return predicados;
//	}
	
	/**
	 * Metodo que recibe un map con campo/valor para unirlos en un where
	 * @param <T>
	 * @param params
	 * @return
	 */
//	private <T> Specification<T> addLikeOrEqualsWhereOrMap(final Map<String, Object> params) {
//
//		Specifications<T> predicados = Specifications.where(null);
//
//		params.entrySet().parallelStream().forEach(a -> {
//			
//			if(a.getValue() instanceof String) {
//				predicados.and((rt, qry, cb) -> {
//					rt.fetch("roles", JoinType.INNER);
//					return cb.or(cb.like(cb.upper(rt.get(a.getKey())), a.getValue().toString().toUpperCase()));
//				});
//			}else {
//				predicados.and((rt, qry, cb) -> {
//					rt.fetch("roles", JoinType.INNER);
//					return cb.or(cb.equal(cb.upper(rt.get(a.getKey())), a.getValue()));
//				});
//			}
//		});
//
//		return predicados;
//	}



}
