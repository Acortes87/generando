package es.acm.generando.loguinApi.mappers;


import es.acm.generando.loguinApi.dto.UsuarioDto;
import es.acm.generando.loguinApi.model.Usuario;

public class UsuarioDtoMapper {

	public static UsuarioDto build(Usuario user) {
		UsuarioDto as = new UsuarioDto();
		as.setNombre(user.getNombre());
		as.setApell1(user.getApell1());
		as.setApell2(user.getApell2());
		
		
		return as;
	}

//	private static Set<? extends GrantedAuthority> getAuthorities(Usuario retrievedUser) {
//		List<Role> roles = retrievedUser.getRoles();
//
//		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
//
//		roles.forEach(role -> authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getCodRole())));
//
//		return authorities;
//	}
}
