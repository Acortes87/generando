package es.acm.generando.loguinApi.dto;

import java.util.List;

public class UsuarioDto {

	private Long idUsu;

	private String apell1;

	private String apell2;

	private String codIdenti;

	private String codUsu;

	private String direccion;

	private String userName;

	private String nombre;

	private String password;

	private List<RoleDto> roles;

	public UsuarioDto() {

	}

	public UsuarioDto(Long idUsu, String apell1, String apell2, String codIdenti, String codUsu, String direccion,
			String userName, String nombre, String password) {
		super();
		this.idUsu = idUsu;
		this.apell1 = apell1;
		this.apell2 = apell2;
		this.codIdenti = codIdenti;
		this.codUsu = codUsu;
		this.direccion = direccion;
		this.userName = userName;
		this.nombre = nombre;
		this.password = password;
	}

	public Long getIdUsu() {
		return idUsu;
	}

	public String getApell1() {
		return apell1;
	}

	public String getApell2() {
		return apell2;
	}

	public String getCodIdenti() {
		return codIdenti;
	}

	public String getCodUsu() {
		return codUsu;
	}

	public String getDireccion() {
		return direccion;
	}

	public String getUserName() {
		return userName;
	}

	public String getNombre() {
		return nombre;
	}

	public String getPassword() {
		return password;
	}

	public List<RoleDto> getRoles() {
		return roles;
	}

	public void setIdUsu(Long idUsu) {
		this.idUsu = idUsu;
	}

	public void setApell1(String apell1) {
		this.apell1 = apell1;
	}

	public void setApell2(String apell2) {
		this.apell2 = apell2;
	}

	public void setCodIdenti(String codIdenti) {
		this.codIdenti = codIdenti;
	}

	public void setCodUsu(String codUsu) {
		this.codUsu = codUsu;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRoles(List<RoleDto> roles) {
		this.roles = roles;
	}

}
