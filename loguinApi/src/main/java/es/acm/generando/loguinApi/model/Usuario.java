package es.acm.generando.loguinApi.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usuarios database table.
 * 
 */
@Entity
@Table(name="usuarios")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_USU", unique=true, nullable=false)
	private Long idUsu;

	@Column(length=45)
	private String apell1;

	@Column(length=45)
	private String apell2;

	@Column(name="COD_IDENTI", length=9)
	private String codIdenti;

	@Column(name="COD_USU", length=8)
	private String codUsu;

	@Column(name="DIRECCION", length=45)
	private String direccion;
	
	@Column(name="USER_NAME", nullable=false, length=45)
	private String userName;

	@Column(name="NOMBRE", length=45)
	private String nombre;

	@Column(name="PASSWORD", nullable=false, length=70)
	private String password;
	
	//bi-directional many-to-many association to Role
	@JoinTable(
		name="ROL_USU"
		, joinColumns={
			@JoinColumn(name="ID_USU", referencedColumnName="ID_USU")
			}
		, inverseJoinColumns={
			@JoinColumn(name="ID_ROLE", referencedColumnName="ID_ROLE")
			}
		)
	@ManyToMany
	private List<Role> roles;

	//bi-directional many-to-one association to TipoEntidad
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name="TIPO_ENTIDAD", nullable=false)
	private TipoEntidad tipoEntidadBean;

	public Usuario() {
	}

	public Long getIdUsu() {
		return this.idUsu;
	}

	public void setIdUsu(Long idUsu) {
		this.idUsu = idUsu;
	}

	public String getApell1() {
		return this.apell1;
	}

	public void setApell1(String apell1) {
		this.apell1 = apell1;
	}

	public String getApell2() {
		return this.apell2;
	}

	public void setApell2(String apell2) {
		this.apell2 = apell2;
	}

	public String getCodIdenti() {
		return this.codIdenti;
	}

	public void setCodIdenti(String codIdenti) {
		this.codIdenti = codIdenti;
	}

	public String getCodUsu() {
		return this.codUsu;
	}

	public void setCodUsu(String codUsu) {
		this.codUsu = codUsu;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public TipoEntidad getTipoEntidadBean() {
		return this.tipoEntidadBean;
	}

	public void setTipoEntidadBean(TipoEntidad tipoEntidadBean) {
		this.tipoEntidadBean = tipoEntidadBean;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}