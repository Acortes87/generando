package es.acm.generando.loguinApi.mappers;

import java.util.List;
import java.util.stream.Collectors;

import es.acm.generando.loguinApi.dto.RoleDto;
import es.acm.generando.loguinApi.model.Role;



public class RoleMapper {

	public static RoleDto roleEntyToDto(Role RoleIn) {

		RoleDto role;
		role = new RoleDto();
		role.setIdRole(RoleIn.getIdRole());
		role.setDescripcion(RoleIn.getDescripcion());
		return role;
	}

	public static List<RoleDto> listRoleEntyToDto(List<Role> lstRoleIn) {

		return lstRoleIn.stream().map(RoleMapper::roleEntyToDto).collect(Collectors.toList());
	}

	public static List<Role> listRoleDtoToEnty(List<RoleDto> lstRoleIn) {

		return lstRoleIn.stream().map(RoleMapper::roleDtoToEnty).collect(Collectors.toList());
		
	}

	
	public static Role roleDtoToEnty(RoleDto RoleIn) {

		Role role = new Role();
		role.setIdRole(RoleIn.getIdRole());
		role.setDescripcion(RoleIn.getDescripcion());
		return role;
	}

}
