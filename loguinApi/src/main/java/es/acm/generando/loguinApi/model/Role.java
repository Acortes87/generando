package es.acm.generando.loguinApi.model;


import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@Table(name="ROLES")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ROLE", unique=true, nullable=false)
	private Long idRole;
	
	@Column(name="COD_ROLE", nullable=false, length=15)
	private String codRole;

	@Column(name="DESCRIPCION", nullable=false, length=45)
	private String descripcion;

	public String getCodRole() {
		return codRole;
	}

	public void setCodRole(String codRole) {
		this.codRole = codRole;
	}

	//bi-directional many-to-many association to Usuario
	@ManyToMany(mappedBy="roles")
	private List<Usuario> usuarios;


	public Long getIdRole() {
		return this.idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}
	public Role getSetIdRole(Long idRole) {
		this.idRole = idRole;
		return this;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

}