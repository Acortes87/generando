package es.acm.generando.loguinApi.dto;

public class RoleDto {
	
	private Long idRole;
	
	private String codRole;
	
	public RoleDto() {
		
	}

	public RoleDto(Long idRole, String codRole, String descripcion) {
		super();
		this.idRole = idRole;
		this.codRole = codRole;
		this.descripcion = descripcion;
	}

	public Long getIdRole() {
		return idRole;
	}

	public String getCodRole() {
		return codRole;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public void setCodRole(String codRole) {
		this.codRole = codRole;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private String descripcion;

}
